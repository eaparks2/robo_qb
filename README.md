Team: Alpha Wolf Squadron

Team Members: Ryan Kastl, rmkastl2
              Matt Jones, msj2
              Ye-Ji (Diana) Mun, yejimun2
              Eric Parks, eaparks2
              
IMPORTANT NOTICE: This document explains the current contents of the Robo_QB repository and what each content is used for. This document should continue to be updated as the project progresses.

Folders:
    
    ProjectUpdate2
    
    Purpose: Holds V-Rep simulator (.ttt) scenes which demonstrate limited functionality needed for Project Update 2 demo. In addition, provides a basis for which the team's standardized simulation environment can be finalized before distributing play call function tasks to individual members.
        Contents:
      
            conveyorBelt.ttt
            Purpose: Scene which contains generic conveyorBelt object, created from instructions at the following link:
                     http://www.coppeliarobotics.com/helpFiles/en/conveyorBeltTutorial.htm
            
            RoboQB.ttt
            Purpose: Scene which contains conveyorBelt object (from conveyorBelt.ttt) with three UR3 robot arms, a cuboid and two ray sensors, accomplishes Project Update 2 simulation demonstration of movement and sensor feedback.
            Future Intention: We want this .ttt file to eventually be developed into the default environment from which we will each produce our play call function codes. This singular simulation environment will allow each developer to have standardize conveyor, sensor, and robot arm positions so that movements are not relative to personal environments.
            
    
    ProjectUpdate3
    
    Purpose: A repository designated for holding our updated V-Rep simulator scene environment for the RoboQB project.
        Contents:
            
            RoboQB.ttt
            Purpose: The updated V-Rep simulator scene for our project. In this scene, we have defined positions and exemplified forward kinematics to meet the Project Update 3 requirements. Also, we have standardized the starting "Set" position
            from which we expect each RoboQB play to begin; in this way, we prepare team to individually construct code for play execution and easy environment reintegration.
            
    ProjectUpdate4
    
    Purpose: Repository containing repositories for each play's V-Rep simulator scene environment.
        Contents:
            
            Hail Mary
            Purpose: Repository containing V-Rep simulator scene environment for Hail Mary play.
                Contents:
                
                RoboQB_ProjectUpdate_HailMary.ttt
                Purpose: V-Rep scene with simulation demonstration of Hail Mary play.
                
            Hand-Off
            Purpose: Repository containing V-Rep simulator scene environment for Hand-Off play.
                Contents:
                
                RoboQB_ProjectUpdate_HandOff.ttt
                Purpose: V-Rep scene with simulation demonstration of Hand-Off play.
            
            Play Action
            Purpose: Repository containing V-Rep simulator scene environment for Play Action play.
                Contents:
                
                RoboQB_ProjectUpdate_PlayAction.ttt
                Purpose: V-Rep scene with simulation demonstration of Play Action play.
                
            QB Kneel
            Purpose: Repository containing V-Rep simulator scene environment for QB Kneel play.
                Contents:
                
                RoboQB_ProjectUpdate_QBKneel.ttt
                Purpose: V-Rep scene with simulation demonstration of QB Kneel play.
                
            QB Sneak
            Purpose: Repository containing V-Rep simulator scene environment for QB Sneak play.
                Contents:
                
                RoboQB_ProjectUpdate_QBSneak.ttt
                Purpose: V-Rep scene with simulation demonstration of QB Sneak play.